import logo from './logo.svg';
import './App.css';
import Nav from './Nav';
import AttendeesList from './AttendeesList';
import LocationForm from './LocationForm';
import { Fragment } from 'react';
import AttendConferenceForm from './AttendConferenceForm';
import ConferenceForm from './ConferenceForm';
import { BrowserRouter, Routes, Route } from "react-router-dom";
import PresentationForm from "./PresentationForm";
import MainPage from './MainPage';


function App(props) {
  if (props.attendees === undefined) {
    return null;
  }
  return (
    <BrowserRouter>
      <Nav />
      <div className="container">
        <Routes>
        <Route index element={<MainPage />} />

          <Route path="locations">
            <Route path="new" element={<LocationForm />} /></Route>

          <Route path="conferences">
            <Route path="new" element={<ConferenceForm />} /></Route>

            <Route path="attendees/new" element={<AttendConferenceForm />} />

            <Route path="conferences/new" element={<AttendeesList />} />

            <Route path="presentations/new" element={<PresentationForm />} />
        </Routes>
      </div>
    </BrowserRouter>
  );
}

export default App;
