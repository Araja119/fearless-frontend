function attendeesList(props) {
    return (
<table className="table table-striped">
  <thead>
    <tr>
      <th scope="col">ID</th>
      <th scope="col">Name</th>
      <th scope="col">Conference</th>
      <th scope="col">HREF</th>
      <th scope="col">Email</th>
    </tr>
  </thead>
  <tbody>
        {props.attendees.map(attendee => {
          return (
            <tr key={attendee.href}>
              <td>{ attendee.id}</td>
              <td>{ attendee.name }</td>
              <td>{ attendee.conference }</td>
              <td>{ attendee.href} </td>
              <td>{ attendee.email}</td>
            </tr>
          );
        })}
        </tbody>
      </table>
    )
}


export default attendeesList;
